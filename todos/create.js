"use strict";

const uuid = require("uuid");
const dynamodb = require("../helpers/dynamodb");

module.exports = async (event, context) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);
  if (typeof data.text !== "string") {
    console.error("Validation Failed");
    return {
      statusCode: 400,
      headers: { "Content-Type": "text/plain" },
      body: "Couldn't create the todo item."
    };
  }

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      id: uuid.v1(),
      text: data.text,
      checked: false,
      createdAt: timestamp,
      updatedAt: timestamp
    }
  };

  // write the todo to the database
  return dynamodb
    .put(params)
    .promise()
    .then(() => {
      // create a response
      const response = {
        statusCode: 200,
        body: JSON.stringify(params.Item)
      };
      return response;
    })
    .catch(error => {
      // handle potential errors
      console.error(error);
      return {
        statusCode: error.statusCode || 501,
        headers: { "Content-Type": "text/plain" },
        body: "Couldn't create the todo item."
      };
    });
};
