"use strict";

const dynamodb = require("../helpers/dynamodb");

module.exports = async (event, context) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE
  };

  // fetch all todos from the database
  return dynamodb
    .scan(params)
    .promise()
    .then(result => {
      // create a response
      const response = {
        statusCode: 200,
        body: JSON.stringify(result.Items)
      };
      return response;
    })
    .catch(error => {
      // handle potential errors
      console.error(error);
      return {
        statusCode: error.statusCode || 501,
        headers: { "Content-Type": "text/plain" },
        body: "Couldn't fetch the todo item."
      };
    });
};
