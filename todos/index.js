"use strict";

module.exports.create = require("./create");
module.exports.list = require("./list");
module.exports.get = require("./get");
module.exports.update = require("./update");
module.exports.delete = require("./delete");
