"use strict";

const dynamodb = require("../helpers/dynamodb");

module.exports = async (event, context) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: event.pathParameters.id
    }
  };

  // delete the todo from the database
  return dynamodb
    .delete(params)
    .promise()
    .then(() => {
      // create a response
      const response = {
        statusCode: 200,
        body: JSON.stringify({})
      };
      return response;
    })
    .then(error => {
      // handle potential errors
      console.error(error);
      return error;
    });
};
