"use strict";

const todos = require("./todos");

module.exports.todo = async (event, context) => {
  const method = event.httpMethod;

  switch (method) {
    case "GET":
      if (event.pathParameters && event.pathParameters.id) {
        return todos.get(event, context);
      } else {
        return todos.list(event, context);
      }
    case "POST":
      return todos.create(event, context);
    case "PUT":
      return todos.update(event, context);
    case "DELETE":
      return todos.delete(event, context);
  }
  // return {
  //   statusCode: 200,
  //   body: JSON.stringify({
  //     message: "Go Serverless v1.0! Your function executed successfully!",
  //     input: event
  //   })
  // };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
