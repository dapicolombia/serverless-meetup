# Serverless Meetup

## Secciones
### Teorico
Tiempo: 20 minutos
Responsable: Brayan Barrios
* Lambda:
    * Buenas practicas: Well architected framework
    * Limitaciones
    * Performance
* DynamoDB
    * Patrones de diseño  

### Practico
Tiempo: 20 minutos
Responsable: Carlos Gomez
Aplicacion que escriba en una base de datos: mongo
* Migracion de backend
https://email.awscloud.com/ttMX0zcOTy0OH0ekZw0005z

### Preguntas
Tiempo: 20 minutos
Responsable: 
* Migraciones
* Casos de uso
* Pendientes:
    * Autenticacion
    * Integracion con servicios de aws

Recursos para preguntas

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact